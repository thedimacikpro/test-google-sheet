<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GoogleSheetStoreRequest;
use App\Models\GoogleSheet;
use Google\Client;
use Google\Service\Drive;
use Illuminate\Support\Str;

class GoogleSheetController extends Controller
{
    public function index()
    {
        return \Inertia\Inertia::render('Table',[
            'google_sheets' => GoogleSheet::all()
        ]);
    }
    public function store(GoogleSheetStoreRequest $request)
    {
        $client = new Client();
        $client->setAuthConfig(public_path() . '/client_credentials.json');
        $client->addScope(Drive::DRIVE);

        $service = new \Google_Service_Sheets($client);

        try{
            $spreadsheet = new \Google_Service_Sheets_Spreadsheet([
                'properties' => [
                    'title' => Str::uuid()
                ],
            ]);
            $spreadsheet = $service->spreadsheets->create($spreadsheet, [
                'fields' => 'spreadsheetId',
            ]);

            // permissions
            $drive = new \Google_Service_Drive($client);
            $newPermission = new \Google_Service_Drive_Permission();

            $newPermission->setType('anyone');
            $newPermission->setRole('writer');

            $spreadsheetId = $spreadsheet->spreadsheetId;

            $drive->permissions->create($spreadsheetId, $newPermission);

            $table = $service->spreadsheets->get($spreadsheet->spreadsheetId);

            $this->setValuesTable($service, $spreadsheetId, ['Имя','Фамилия','Телефон','Текст заявки'],'Sheet1!A1:E');
            $this->setValuesTable($service, $spreadsheetId, [
                $request['first_name'],
                $request['last_name'],
                $request['phone'],
                $request['text'],
            ],'Sheet1!A2:E');

            GoogleSheet::create([
                'link' => $table->spreadsheetUrl,
            ]);
            return back();
        } catch(\Exception $e) {

            echo 'Message: ' .$e->getMessage();
        }
    }

    public function setValuesTable($service, $spreadsheetId, $values, $range)
    {
        $valueRange = new \Google_Service_Sheets_ValueRange();

        $conf = ["valueInputOption" => "RAW"];

        $valueRange->setValues(["values" => $values]);

        $service->spreadsheets_values->update($spreadsheetId, $range, $valueRange, $conf);
    }
}
