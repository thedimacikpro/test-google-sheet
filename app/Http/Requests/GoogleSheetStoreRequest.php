<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GoogleSheetStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            'text'       => 'required',
        ];
    }

    public function  messages()
    {
        return [
            'first_name.required' => 'Имя - обязательное поле',
            'last_name.required'  => 'Фамилия - обязательное поле',
            'phone.required'      => 'Телефон - обязательное поле',
            'text.required'       => 'Текст - обязательное поле',
        ];
    }
}
