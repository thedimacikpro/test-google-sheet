<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('google-sheets')->group(function () {
   Route::post('/',[\App\Http\Controllers\Api\GoogleSheetController::class,'store'])->name('google-sheets.store');
});
